import Vapor
import HarmonicCore
import SwiftyBeaverProvider

/// Called before your application initializes.
public func configure(_ config: inout Config, _ env: inout Environment, _ services: inout Services) throws {

    // Service conf
    if Environment.get("FX_DOCKER") == nil {
        let serverConf = NIOServerConfig.default(hostname: "localhost", port: 9090)
        services.register(serverConf)
    }
    
    // Register routes to the router
    let router = EngineRouter.default()
    try routes(router)
    services.register(router, as: Router.self)

    // Register middleware
    var middlewares = MiddlewareConfig() // Create _empty_ middleware config
    middlewares.use(FileMiddleware.self) // Serves files from `Public/` directory
    middlewares.use(ErrorMiddleware.self) // Catches errors and converts to HTTP response
    services.register(middlewares)
    
    // Setup your destinations
    let console = ConsoleDestination()
    console.minLevel = .info // update properties according to your needs

    let fileDestination = FileDestination()
    let directory = DirectoryConfig.detect()
    fileDestination.logFileURL = URL(fileURLWithPath: directory.workDir)
        .appendingPathComponent("Public", isDirectory: true)
        .appendingPathComponent("server.log", isDirectory: false)
    fileDestination.asynchronously = true

    // Register the logger
    services.register(SwiftyBeaverLogger(destinations: [console, fileDestination]), as: Logger.self)

    // Optional
    config.prefer(SwiftyBeaverLogger.self, for: Logger.self)

}
