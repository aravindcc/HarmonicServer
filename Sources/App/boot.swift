import Vapor
import TradingCore
import HarmonicCore

/// Called after your application has initialized.
public func boot(_ app: Application) throws {
    // Start Quant
    let usingCache = false // ie use gcapi not alpha
    FXCache.shared.dataDirectory = Environment.get("DATACACHE_PATH") ?? FXCache.shared.dataDirectory // if using alpha
    HarmonicAnalyser.bullishWeight = 0.92
    HarmonicAnalyser.bearishWeight = 1.08
    HarmonicAnalyser.errorAllowed  = { (frame: TimeFrame, bullish: Bool) -> Double in
        var rawVal: Double!
        switch frame {
        case .sixtyMinute, .thirtyMinute, .fifteenMinute:
            rawVal = 0.17
        case .daily, .weekly:
            rawVal = 0.25
        default:
            rawVal = 0.05
        }
        return bullish ? rawVal * HarmonicAnalyser.bullishWeight : rawVal * HarmonicAnalyser.bearishWeight
    }
    let logger = try app.make(Logger.self)
    
    let quantTrader = TradeDelegate(app: app)
    quantTrader.logger = logger
    
    logger.info("Starting Quant. Configuring ...")
    let quant = Quant(usingCache, backtest: false)
    quant.trader = quantTrader
    quant.analyseFrames = [TimeFrame.daily, TimeFrame.sixtyMinute, TimeFrame.thirtyMinute]
    quant.chosenFrames = quant.analyseFrames
    quant.slippageAdjust = { pair in
        pair.slippage * 2.2
    }
    quant.SHOULD_YOYO = false
    quant.LAG_TIME = 1
    if let client = quant.getTradingClient() {
        client.getOutputLength = { type in
            switch type {
            case .compact: return 50
            case .full: return 100
            }
        }
    }
    logger.info("Configured Quant!")
    quant.kickOff()
    logger.info("Began Scanning!")
}

extension TradeOrder: Content {}

class TradeDelegate: QuantTrader {
    private var app: Application!
    var logger: Logger?

    public init(app: Application) {
        self.app = app
    }
    
    func trade(order: TradeOrder) {
        do {
            if let enced = try? JSONEncoder().encode(order), let str = String(data: enced, encoding: .utf8) {
                print("NEW TRADE \(str)")
            }
            let host = Environment.get("TRADE_URL") ?? "localhost"
            let port = Environment.get("TRADE_PORT") ?? "8080"
            let url = "http://\(host):\(port)/add"
            let headers = HTTPHeaders([
                ("Content-Type", "application/json")
            ])
            
            let res = try app.client().post(url, headers: headers) { tradeReq in
                try tradeReq.content.encode(order)
            }
            
            res.do { response in
                if response.http.status == .ok {
                    self.logger?.info("Succesfully sent trade tip.")
                } else {
                    self.logger?.error("Failed to send trade tip")
                }
            }.catch { error in
                self.logger?.error("Failed to send trade tip")
            }
        } catch {
            self.logger?.error("Failed to send trade tip")
        }
    }
}
