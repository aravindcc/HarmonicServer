// swift-tools-version:5.1
import PackageDescription

let package = Package(
    name: "HarmonicServer",
    platforms: [
        .macOS(.v10_12),
    ],
    products: [
        .library(name: "HarmonicServer", targets: ["App"]),
    ],
    dependencies: [
        // 💧 A server-side Swift web framework.
        .package(url: "https://github.com/vapor/vapor.git", from: "3.0.0"),

        // 🔵 Swift ORM (queries, models, relations, etc) built on SQLite 3.
        .package(url: "../Core/HarmonicCore", .branch("master")),
        .package(url: "https://github.com/SwiftyBeaver/SwiftyBeaver.git", .exact("1.8.4")),
        .package(url: "https://github.com/vapor-community/swiftybeaver-provider.git", from: "3.0.0")
    ],
    targets: [
        .target(name: "App", dependencies: [ "Vapor", "HarmonicCore", "SwiftyBeaverProvider"]),
        .target(name: "Run", dependencies: ["App"]),
        .testTarget(name: "AppTests", dependencies: ["App"])
    ]
)

